docker registry image
---------------------

You need to create `/srv/registry` and `/srv/auth` folders and htpasswd file (using htpassword from apache2-utils package):
```
$ mkdir /srv/registry
$ chown 101:101 /srv/registry
$ mkdir /srv/auth
$ htpasswd -cB /srv/auth/htpasswd registry
```

Start the registry using docker compose (in the folder with the docker-compose.yml):
```
$ docker-compose up -d
```
