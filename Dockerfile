FROM registry.sindominio.net/debian

RUN apt-get update && \
    apt-get install -y docker-registry

USER docker-registry
ENTRYPOINT ["/usr/bin/docker-registry"]
CMD ["serve", "/etc/docker/registry/config.yml"]
